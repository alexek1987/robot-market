import { atom, selector } from "recoil";

export const cartItems = atom({
  key: "cartItems",
  default: [],
});

export const cartItemsTotal = selector({
  key: "cartItemsTotal",
  get: ({ get }) => {
    const cart = get(cartItems);
    const totalPrice = cart.reduce((total, item) => {
      return Number(item.price) + total;
    }, 0);

    return totalPrice;
  },
});
