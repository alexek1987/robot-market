import { atom } from "recoil";

export const allRobotsState = atom({
  key: "allRobots",
  default: [],
});
