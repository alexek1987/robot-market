import "./Product.css";
import moment from "moment";
import { useState } from "react";
import placeholder from "./assets/placeholder.png";
import CurrencyFormat from "react-currency-format";
import { useRecoilState } from "recoil";
import { cartItems } from "./Atoms/cartItems";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import RemoveShoppingCartIcon from "@material-ui/icons/RemoveShoppingCart";
import { allRobotsState } from "./Atoms/allRobotsState";
import { useLocation } from "react-router-dom";
import styled from "styled-components";

function Product({ robot, isCart, cartIndex, index }) {
  const location = useLocation();
  const [cartItemsArray, setCartItemsArray] = useRecoilState(cartItems);
  const [allRobots, setAllRobots] = useRecoilState(allRobotsState);
  let stock = robot.stock;

  const addToCart = () => {
    setCartItemsArray([...cartItemsArray, robot]);

    // let copyOfAllRobots = [...allRobots];
    // let robotStockToSet = copyOfAllRobots[index].stock;
    // let newVal = (robotStockToSet -= 1);
    // copyOfAllRobots[index].stock = newVal;
    // setAllRobots(copyOfAllRobots);
    setAllRobots((prev) => {
      let copy = JSON.parse(JSON.stringify(prev));
      copy[index].stock -= 1;

      return copy;
    });
  };

  const removeFromCart = () => {
    const copyOfCartItems = [...cartItemsArray];
    copyOfCartItems.splice(cartIndex, 1);
    setCartItemsArray(copyOfCartItems);
  };

  const [loading, setLoading] = useState(true);
  const [failed, setFailed] = useState(false);

  const onLoadHandler = () => {
    setLoading(false);
  };

  const onErrorHandler = () => {
    setFailed(true);
    setLoading(false);
  };

  return (
    <ProductContainer>
      <ProductInfo>
        <RobotName>{robot.name}</RobotName>
        <ProductPrice>
          <strong>
            <CurrencyFormat
              displayType={"text"}
              value={Math.trunc(robot.price)}
              thousandSeparator={true}
              prefix={"฿"}
            />
          </strong>
        </ProductPrice>
        {location.pathname != "/checkout" && (
          <RobotStock>Stock: {robot.stock}</RobotStock>
        )}

        <RobotMaterial>Material: {robot.material}</RobotMaterial>

        {!isCart && (
          <RobotCreatedAt>
            Created at: {robot.createdAt && moment(robot.createdAt).format("l")}
          </RobotCreatedAt>
        )}
      </ProductInfo>
      {loading && <div>Loading...</div>}
      {failed ? (
        <img
          style={{ height: "235px", marginBottom: "14px" }}
          src={placeholder}
        />
      ) : (
        <img
          className="product__img"
          src={failed ? placeholder : robot.image}
          onError={onErrorHandler}
          onLoad={onLoadHandler}
        />
      )}

      {isCart ? (
        <>
          <ProductIcons>
            {stock > 1 && (
              <AddShoppingCartIcon
                className="increment__product__icon"
                onClick={addToCart}
              />
            )}

            <RemoveShoppingCartIcon
              className="decrement__product__icon"
              onClick={removeFromCart}
            />
          </ProductIcons>
        </>
      ) : (
        <AddProductButton disabled={stock === 0} onClick={addToCart}>
          {stock === 0 ? "Sold out" : "Add to Cart"}
        </AddProductButton>
      )}
    </ProductContainer>
  );
}

export default Product;

const ProductContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  max-height: 600px;
  min-width: 100px;
  margin: 10px;
  margin-bottom: 10px;
  background-color: white;
  padding: 20px;
  width: 100%;
  z-index: 1;
  margin-top: 100px;
`;

const ProductInfo = styled.div`
  height: 100px;
  margin-bottom: 100px;
`;
const ProductIcons = styled.div`
  display: flex;
`;

const ProductPrice = styled.p``;
const RobotStock = styled.p``;
const RobotMaterial = styled.p``;
const RobotCreatedAt = styled.p``;
const RobotName = styled.p``;

const AddProductButton = styled.button`
  background-color: #f0c14b;
  border: 1px solid;
  padding: 7px;
  border-color: #a88734 #9c7e31 #846a29;
  border-radius: 3px;
`;
