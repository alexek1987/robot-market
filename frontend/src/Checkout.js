import { cartItems, cartItemsTotal } from "./Atoms/cartItems";
import { useRecoilValue } from "recoil";
import Product from "./Product";
import CurrencyFormat from "react-currency-format";
import styled from "styled-components";

function Checkout() {
  const cartItemsArray = useRecoilValue(cartItems);
  const total = useRecoilValue(cartItemsTotal);
  const cartCount = cartItemsArray.length;

  return (
    <CheckoutContainer>
      <CheckoutBanner>
        <BannerImage />
      </CheckoutBanner>
      <CheckoutCartInfo>
        Total items:&nbsp;
        <strong>{cartItemsArray.length}</strong>
        (Subtotal:&nbsp;
        <strong>
          <CurrencyFormat
            displayType={"text"}
            value={Math.trunc(total)}
            thousandSeparator={true}
            prefix={"฿"}
          />
        </strong>
        )
      </CheckoutCartInfo>
      {cartCount === 0 ? (
        <EmptyCartMessage>
          <EmptyCartHeader>Your Shopping Cart is empty</EmptyCartHeader>
          <EmptyCartText>
            You have no items in your cart. To buy one or more items click "Add
            to Cart" on the front page.
          </EmptyCartText>
        </EmptyCartMessage>
      ) : (
        <CheckoutCartProducts>
          {cartItemsArray.map((item, index) => (
            <CheckoutCartColumns>
              <Product
                isCart={true}
                robot={item}
                cartIndex={index}
                index={index}
              />
            </CheckoutCartColumns>
          ))}
        </CheckoutCartProducts>
      )}
    </CheckoutContainer>
  );
}

export default Checkout;

const CheckoutContainer = styled.div`
  background-color: #fbfbfd;
`;
const CheckoutBanner = styled.div`
  margin-bottom: 3px;
`;

const CheckoutCartInfo = styled.div`
  margin-top: 20px;
  margin-left: 83px;
  font-size: 20px;
`;
const EmptyCartMessage = styled.div`
  margin-top: 20px;
  margin-left: 83px;
`;
const EmptyCartHeader = styled.h2`
  font-size: 25px;
  font-weight: 800;
`;
const EmptyCartText = styled.p`
  font-size: 20px;
`;
const CheckoutCartProducts = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
`;
const CheckoutCartColumns = styled.div`
  height: 540px;
  flex-basis: 50%;
  -ms-flex: auto;
  width: 299px;
  position: relative;
  padding: 1px;
  box-sizing: border-box;
`;

const BannerImage = styled.img`
  width: 100%;
  height: auto;
`;

BannerImage.defaultProps = {
  src:
    "https://cdn.shopify.com/s/files/1/0281/5446/2242/files/R2D2-Banner_1024x1024.jpg?v=1601312440",
};
