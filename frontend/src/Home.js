import { useState, useEffect } from "react";
import Product from "./Product";
import "./Home.css";
import { cartItems } from "./Atoms/cartItems";
import { allRobotsState } from "./Atoms/allRobotsState";
import { useRecoilValue, useRecoilState } from "recoil";

function Home() {
  const cartItemsArray = useRecoilValue(cartItems);
  const [allRobots, setAllRobots] = useRecoilState(allRobotsState);
  const [filterMaterial, setFilterMaterial] = useState("All");

  useEffect(() => {
    fetch("http://localhost:8000/api/robots")
      .then((result) => result.json())
      .then((data) => {
        setAllRobots(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div className="home">
      <div className="home__filter__materials">
        <span class="custom__badge badge badge-light">Filter by:</span>
        <select
          className="form-control form-control-md"
          onChange={(e) => setFilterMaterial(e.target.value)}
          value={filterMaterial}
        >
          <option value="All">All</option>
          <option value="Soft">Soft</option>
          <option value="Granite">Granite</option>
          <option value="Frozen">Frozen</option>
          <option value="Steel">Steel</option>
          <option value="Plastic">Plastic</option>
          <option value="Wooden">Wooden</option>
          <option value="Cotton">Cotton</option>
          <option value="Rubber">Rubber</option>
          <option value="Metal">Metal</option>
          <option value="Fresh">Fresh</option>
          <option value="Concrete">Concrete</option>
          <option value="Rubber">Rubber</option>
        </select>
      </div>

      {cartItemsArray.length >= 5 && (
        <div className="cart__alert">You put 5 products in your cart!</div>
      )}

      <div className="container">
        <div className="home__row row">
          {allRobots.map((robot, index) =>
            filterMaterial === robot.material ? (
              <div className="col-sm-4">
                <Product index={index} robot={robot} />
              </div>
            ) : filterMaterial === "All" ? (
              <div className="col-sm-4">
                <Product index={index} robot={robot} />
              </div>
            ) : null
          )}
        </div>
      </div>
    </div>
  );
}

export default Home;
