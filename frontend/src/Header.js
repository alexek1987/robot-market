import { Link } from "react-router-dom";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import { cartItems } from "./Atoms/cartItems";
import { useRecoilValue } from "recoil";
import styled from "styled-components";

function Header() {
  const cartItemsArray = useRecoilValue(cartItems);

  return (
    <HeaderContainer>
      <HeaderLeft>
        <HeaderText>
          <Link style={{ color: "white", textDecoration: "none" }} to="/">
            Robot Market
          </Link>
        </HeaderText>
      </HeaderLeft>
      <HeaderRight>
        <Link style={{ color: "white", textDecoration: "none" }} to="/checkout">
          <ShoppingBasketIcon />
          <HeaderCartCount>{cartItemsArray.length}</HeaderCartCount>
        </Link>
      </HeaderRight>
    </HeaderContainer>
  );
}

export default Header;

const HeaderContainer = styled.div`
  background-color: #444444;
  display: flex;
  align-items: center;
  position: sticky;
  top: 0;
  justify-content: space-between;
  z-index: 100;
`;
const HeaderLeft = styled.div`
  margin-left: 30px;
`;
const HeaderRight = styled.div`
  cursor: pointer;
  font-size: 30px;
  margin-bottom: 8px;
  margin-right: 30px;
`;

const HeaderCartCount = styled.span`
  font-size: 22px;
  margin-left: 5px;
`;
const HeaderText = styled.h1`
  cursor: pointer;
  font-size: 30px;
`;
