import express from "express";
import {
  getAllRobots,
  updateStockById,
} from "../controllers/robotControllers.js";

// init router
const router = express.Router();

router.get("/", getAllRobots); // '/robots'
router.put("/:id/stock", updateStockById); // '/robots/:id/stock'

export default router;
