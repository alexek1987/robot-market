import express from "express";
import cors from "cors";
import robotRoutes from "./routes/robotRoutes.js";

// intit app
const app = express();

// middleware
app.use(express.json()); // body parser
app.use(cors({ origin: "*" })); // enable http requests

// configure db

// connect to db

// routes (htttp requests)
app.get("/", (req, res, next) =>
  res.status(200).json({ message: "Our Robots RESTful API" })
);
app.use("/robots", robotRoutes);

// server is listening
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`✅ Server is listening in port ${PORT}`));
