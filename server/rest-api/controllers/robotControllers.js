export const getAllRobots = (req, res, next) => {
  res.status(200).json({ message: "found all robots" });
};

export const updateStockById = (req, res, next) => {
  const { id } = req.params;
  res.status(200).json({ message: "robot stock updated", robotId: id });
};
