const express = require("express");
const cors = require("cors");
const faker = require("faker");

const app = express();
app.use(cors());

const PORT = 8000;
const MAX_ROBOTS = 100;

app.get("/api/robots", (_req, res) => {
  let robotList = [];

  for (let i = 0; i < MAX_ROBOTS; i++) {
    let name = faker.name.firstName() + " " + faker.name.lastName();
    let image = `https://robohash.org/${name}.png`;
    let price = faker.finance.amount();
    let stock = faker.random.number({ max: 10, min: 0 });
    let createdAt = faker.date.past();
    let material = faker.commerce.productMaterial();

    robotList.push({
      name,
      image,
      price,
      stock,
      createdAt,
      material,
    });
  }

  return res.json({
    data: robotList,
  });
});

app.get("/", (_req, res) => {
  return res.send("Nothing to see here... Just go to `/robots/`");
});

app.listen(PORT, () => console.log(`server is running on PORT: ${PORT}`));
